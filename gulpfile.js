var gulp 		= require('gulp'),
	sass 		= require('gulp-sass'),
	browserSync = require('browser-sync'),
	concat 		= require('gulp-concat'),
	uglify 		= require('gulp-uglifyjs'),
	cssnano 	= require('gulp-cssnano'),
	rename 		= require('gulp-rename'),
	del 		= require('del'),
	imagemin 	= require('gulp-imagemin'),
	pngquant 	= require('imagemin-pngquant'),
	cache 		= require('gulp-cache'),
	prefixer    = require('gulp-autoprefixer');

gulp.task('sass', function() {
	return gulp.src('assets/sass/**/*.+(scss|sass)')
	.pipe(sass())
	.pipe(prefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
	.pipe(gulp.dest('assets/css'))
	.pipe(browserSync.reload({stream: true}))
});

gulp.task('scripts', function() {
	return gulp.src([
		'assets/libs/jquery/dist/jquery.min.js',
		'assets/libs/magnific-popup/dist/jquery.magnific-popup.min.js'
	])
	.pipe(concat('libs.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('assets/js'));
});

gulp.task('css-libs', ['sass'], function() {
	return gulp.src('assets/css/libs.css')
	.pipe(cssnano())
	.pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest('assets/css'));
});

gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'assets'
		},
		notify: false
	});
});

gulp.task('clean', function() {
	return del.sync('build');
});

gulp.task('clear', function() {
	return cache.clearAll();
});

gulp.task('img', function() {
	return gulp.src('assets/img/**/*')
	.pipe(cache(imagemin({
		interlaced: true,
		progressive: true,
		svgoPlugins: [{removeViewBox: false}],
		use: [pngquant()]
	})))
	.pipe(gulp.dest('build/img'));
});

gulp.task('watch', ['browser-sync', 'css-libs', 'scripts'], function() {
	gulp.watch('assets/sass/**/*.+(scss|sass)', ['sass']);
	gulp.watch('assets/*.html', browserSync.reload);
	gulp.watch('assets/js/**/*.js', browserSync.reload);
});

gulp.task('build', ['clean', 'img', 'sass', 'scripts'], function() {
	var buildCss = gulp.src([
		'assets/css/main.css',
		'assets/css/libs.min.css'
	])
	.pipe(gulp.dest('build/css'));

	var buildFonts = gulp.src('assets/fonts/**/*')
	.pipe(gulp.dest('build/fonts'));

	var buildJs = gulp.src('assets/js/**/*')
	.pipe(gulp.dest('build/js'));

	var buildHtml = gulp.src('assets/*.html')
	.pipe(gulp.dest('build'));
});